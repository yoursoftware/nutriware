<!DOCTYPE HTML>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="libs/css/main.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="libs/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="libs/css/bootstrap-theme.min.css">

<style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #006400;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }

      .form-signin{
        background: rgb(242,245,246); /* Old browsers */
background: -moz-linear-gradient(top,  rgba(242,245,246,1) 0%, rgba(227,234,237,1) 37%, rgba(200,215,220,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(242,245,246,1)), color-stop(37%,rgba(227,234,237,1)), color-stop(100%,rgba(200,215,220,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f5f6', endColorstr='#c8d7dc',GradientType=0 ); /* IE6-9 */

      }


      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
      .img-responsive{
      	display: initial !important;
		max-width: 40% !important;
		height: auto !important;
		}
      }

    </style>

<title>Nutriware</title>
</head>
<body>
<div class="login">


	<form class="form-signin" role="form" action="login.php" method="post">

	<p style="text-align:center !important; color:#EF9C35;">
		<p style="text-align:center !important;"><img src="logo_nutri.png" class="img-responsive"></p>
		<h2 style="text-align:center !important; color:#FAEBD7;display:none;"><i>Nutriware</i></h2>
	</p>
	<br>

	


        <p style="text-align:center;">
        <input id="usuario" type="text" name="usuario" maxlength="30" class="form-control" placeholder="Login (CPF)" required autofocus>
        </p>
        <p style="text-align:center;">
        <input id="senha" name="senha" type="password" maxlength="20" class="form-control" placeholder="Senha" required>
        </p>
        <p style="text-align:center;">
        <button class="btn btn-warning btn-primary btn-block" type="submit" >Entrar</button><br>
        </p>
        <p style="text-align:center;"><a href="cadastro.html" style="text-shadow: 0px 0px 5px rgba(150, 150, 150, 1);"><font color='black'>Faça aqui seu cadastro Nutricionista.</a></p>
        <p style="text-align:center;"><a href="Recupera_email.php" style="text-shadow: 0px 0px 5px rgba(150, 150, 150, 1);"><font color='black'>Esqueceu a sua senha? Clique aqui.</a></p><br>
        <p style="font-size:9px !important;text-align:center;">2014 - Fábrica de Software YourSoftware S.A.</p>

 
 </form>




</div>
<!-- Carregamento dos scripts -->

<script src="libs/js/jquery.min.js"></script>
<script src="libs/js/jquery.mask.min.js"></script>
<script src="libs/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  

  $("#usuario").mask('00000000000', {reverse: true});
});
</script>
</body>






</html>
