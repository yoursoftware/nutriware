<html lang="pt-br">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="libs/css/main.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="libs/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="libs/css/bootstrap-theme.min.css">

<title>Nutriware</title>
</head>
<body>

 <b>
  <div class="container"> 
     <div id="logo" class="logo pull-right"><p style="color:#EF9C35; font-size:24px; margin-top:10px; float:left;"><i><b>Nutriware</b></i></p><img style="float:right;" src="logo_nutri.png" width="120px" height="auto"></div>
    <form action="cadastrar.php" method="post">
<div class="content">
<h3><i>Cadastrar novo Cliente:</i></h3>


<div class="input-group">
  <span class="input-group-addon">Meu cpf:</span>
  <input type="text" name="cpfNutricionista"  id="cpfNutricionista" class="form-control" placeholder="" value ="<?php echo $_SESSION['usuarioID'];?>" maxlength="50" required>
</div>
<div class="input-group">
  <span class="input-group-addon">Nome:</span>
  <input type="text" name="nome"  id="nome" class="form-control" placeholder="" maxlength="50" required>
</div>

<div class="input-group">
  <span class="input-group-addon">CPF:</span>
  <input type="text" name="cpf" id="cpf" class="form-control" placeholder="" maxlength="20" required>
</div>
<div class="input-group">
  <span class="input-group-addon">Senha:</span>
  <input type="password" name="senha" id="senha" class="form-control" placeholder="" maxlength="10" required>
</div>
<div class="input-group">
  <span class="input-group-addon">Data de nascimento:</span>
  <input type="text" name="datanasc" id="datanasc" class="form-control" maxlength="10" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Tipo do Usuário:</span>
  <select class="form-control" name="tipo" id="tipo">
  <option value="2" >Cliente</option>
  
</select>
</div>

<br />
<h4>Endereço:</h4>
<br />
<div class="input-group">
  <span class="input-group-addon">CEP:</span>
  <input type="text" name="cep" id="cep" class="form-control" maxlength="10" placeholder="">
</div>
<div class="input-group">
  <span class="input-group-addon">Logradouro:</span>
  <input type="text" name="logradouro" id="logradouro" class="form-control" maxlength="70" placeholder="">
</div>
<div class="input-group">
  <span class="input-group-addon">Bairro:</span>
  <input type="text" name="bairro" id="bairro" class="form-control" maxlength="50" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Cidade:</span>
  <input type="text" name="cidade" id="cidade" class="form-control" maxlength="50" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Número:</span>
  <input type="text" name="num" id="num" class="form-control" maxlength="10" placeholder="">
</div>



<div class="input-group">
  <span class="input-group-addon">Telefone Fixo:</span>
  <input type="text" name="telfixo" id="telfixo" class="form-control" maxlength="15" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Celular:</span>
  <input type="text" name="celular" id="celular" class="form-control" maxlength="15" placeholder="">
</div>
<div class="input-group">
  <span class="input-group-addon">Peso(kg):</span>
  <input type="text" name="peso" id="peso" class="form-control" maxlength="15" placeholder="">
</div>
<div class="input-group">
  <span class="input-group-addon">Altura(cm):</span>
  <input type="text" name="altura" id="altura" class="form-control" maxlength="15" placeholder="">
</div>
<br />

<br />

<br />
<p style="text-align:center;">
<button class="btn btn-lg btn-primary btn-warning" type="submit">Cadastrar</button>
</p>
</form>

  </div>
<footer>

<div class="panel panel-default">
 <div class="panel-footer pull-center">2014 - Fábrica de Software YourSoftware S.A. - Todos os direitos reservados.</div>
</div>


</footer>

</div>

<!-- Carregamento dos scripts -->




<script src="libs/js/jquery.min.js"></script>
<script src="libs/js/bootstrap.min.js"></script>
<script src="libs/js/jquery.mask.min.js"></script>

<script>
$(document).ready(function(){
  
  //Máscaras
  //$("#cpf").mask('000.000.000-00', {reverse: true});
  $("#datanasc").mask('00/00/0000');
  $("#cep").mask('00000-000');
  $("#telfixo").mask('(00) 0000-0000');
  $("#celular").mask('(00) 0000-0000');
  $("#cpf").mask('00000000000', {reverse: true});
  $("#peso").mask('00,00kg');
  $("#altura").mask('0,00m');

});

</script>
</body>