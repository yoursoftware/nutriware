<?php
include_once("db.php");
//echo $_SESSION['usuarioNome'];

?>
<html lang="pt-br">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="libs/css/main.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="libs/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="libs/css/bootstrap-theme.min.css">

<title>Nutriware</title>
</head>
<body>

  <div class="container"> 
     <div id="logo" class="logo pull-right"><p style="color:#EF9C35; font-size:24px; margin-top:10px; float:left;"><i><b>Nutriware</b></i></p><img style="float:right;" src="logo_nutri.png" width="120px" height="auto"></div>
    <form action="marcar_consulta2.php" method="post">
<div class="content">
<h3><i>Marcar Consulta:</i></h3>

<div class="input-group">
  <span class="input-group-addon">Meu Cpf:</span>
  <input type="text" name="cpfNutricionista"  id="cpfNutricionista" class="form-control" placeholder="" maxlength="50" required>
</div>

<div class="input-group">
  <span class="input-group-addon">Cpf Cliente:</span>
  <input type="text" name="cpfCliente"  id="cpfCliente" class="form-control" placeholder="" maxlength="50" required>
</div>

<div class="input-group">
  <span class="input-group-addon">Data:</span>
  <input type="text" name="data" id="data" class="form-control" placeholder="" maxlength="20" required>
</div>
<div class="input-group">
  <span class="input-group-addon">Hora:</span>
  <input type="password" name="hora" id="hora" class="form-control" placeholder="" maxlength="10" required>
</div>
<div class="input-group">
  <span class="input-group-addon">Local:</span>
  <input type="text" name="local" id="local" class="form-control" maxlength="10" placeholder="">
</div>


<br />

<br />

<br />
<p style="text-align:center;">
<button class="btn btn-lg btn-primary btn-warning" type="submit">Marcar Consulta</button>
</p>
</form>

<script src="libs/js/bootstrap.min.js"></script>
<script src="libs/js/jquery.min.js"></script>
<script src="libs/js/jquery.mask.min.js"></script>

<script>
$(document).ready(function(){
	//Máscaras
  $("#data").mask('00/00/0000');
  $("hora").mask('00:00h');

  </script>
