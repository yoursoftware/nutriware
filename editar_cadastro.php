<?php
require_once("db.php");
$nome = $_SESSION['usuarioNome'];

$result = mysql_query('SELECT * FROM usuario WHERE usuario.nome = '.'\''.$nome.'\'');

while ($row = mysql_fetch_assoc($result)) {

   $cpf=$row["cpf"];
   $senha=$row["senha"];
   $datanasc=$row["datanasc"];
   $tipo=$row["tipo"];
   $cep=$row["cep"];
   $logradouro=$row["logradouro"];
   $bairro=$row["bairro"];
   $cidade=$row["cidade"];
   $numero=$row["num"];
   $complemento=$row["complemento"];
   $telfixo=$row["telfixo"];
   $celular=$row["celular"];


}

mysql_free_result($result);



?>
<!-- Carregamento dos scripts -->
<script src="libs/js/bootstrap.min.js"></script>
<script src="libs/js/jquery.min.js"></script>
<script src="libs/js/jquery.mask.min.js"></script>

<script>
$(document).ready(function(){
  
  
  $("#cpf").val("<?php echo $cpf; ?>");
  $("#nome").val("<?php echo $nome; ?>");
  $("#senha").val("<?php echo $senha; ?>");
  $("#datanasc").val("<?php echo $datanasc; ?>");
  $("#tipo").val("<?php echo $tipo; ?>");
  $("#cep").val("<?php echo $cep; ?>");
  $("#logradouro").val("<?php echo $logradouro; ?>");
  $("#bairro").val("<?php echo $bairro; ?>");
  $("#cidade").val("<?php echo $cidade; ?>");
  $("#numero").val("<?php echo $numero; ?>");


  $("#complemento").val("<?php echo $complemento; ?>");


  //Máscaras
  $("#cpf").mask('000.000.000-00', {reverse: true});
  $("#datanasc").mask('00/00/0000');
  $("#cep").mask('00000-000');
  $("#telfixo").mask('(00) 0000-0000');
  $("#celular").mask('(00) 0000-0000');




});
</script>
<h3><i>Editar usuário</i></h3>
<form action="atualizar_cadastro.php" method="post">
<div class="input-group">
  <span class="input-group-addon">Nome:</span>
  <input type="text" name="nome"  id="nome" class="form-control" placeholder="" maxlength="50" required>
  <?php $_SESSION['usuarioNome']; ?>
</div>

<div class="input-group">
  <span class="input-group-addon">CPF:</span> 
  <input type="text" name="cpf" id="cpf" class="form-control" placeholder="" maxlength="20" required>
</div>
<div class="input-group">
  <span class="input-group-addon">Senha:</span>
  <input type="password" name="senha" id="senha" class="form-control" placeholder="" maxlength="10" required>
</div>
<div class="input-group">
  <span class="input-group-addon">Data de nascimento:</span>
  <input type="text" name="datanasc" id="datanasc" class="form-control" maxlength="10" placeholder="">
</div>
<br />
<h4>Endereço:</h4>
<br />
<div class="input-group">
  <span class="input-group-addon">CEP:</span>
  <input type="text" name="cep" id="cep" class="form-control" maxlength="10" placeholder="">
</div>
<div class="input-group">
  <span class="input-group-addon">Logradouro:</span>
  <input type="text" name="logradouro" id="logradouro" class="form-control" maxlength="70" placeholder="">
</div>
<div class="input-group">
  <span class="input-group-addon">Bairro:</span>
  <input type="text" name="bairro" id="bairro" class="form-control" maxlength="50" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Cidade:</span>
  <input type="text" name="cidade" id="cidade" class="form-control" maxlength="50" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Número:</span>
  <input type="text" name="numero" id="numero" class="form-control" maxlength="10" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Complemento:</span>
  <input type="text" name="complemento" id="complemento" class="form-control" maxlength="50" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Telefone Fixo:</span>
  <input type="text" name="telfixo" id="telfixo" class="form-control" maxlength="15" placeholder="">
</div>

<div class="input-group">
  <span class="input-group-addon">Celular:</span>
  <input type="text" name="celular" id="celular" class="form-control" maxlength="15" placeholder="">
</div>
<br />

<br />
<p style="text-align:center;">
<button class="btn btn-lg btn-primary btn-warning" type="submit">Atualizar</button>
</p>
</form>
